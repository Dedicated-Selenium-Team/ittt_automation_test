<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
Hello,<br><p>
We want to inform you that the project "<b>{{$project_detail[0]->project_name}}</b>" was archived by <b>{{$user_name}}</b>. 
If you believe this was an error, please report to the ITTT admin.
<br><br>
This message was sent to all users who assigned themselves to this project.
<br><br>
<small>This message was sent from a notification-only email address that does not accept incoming email. Please do not reply to this message.</small>
</body>
</html>
